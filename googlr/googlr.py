# -*- coding: utf-8 -*-

from flask import Flask, render_template, make_response
import sqlite3
from contextlib import closing
from flask import g, session
from flask import request, redirect, abort
from flask import flash, url_for
import requests

#################
#  Konfiguracja #
#################

DATABASE = 'googlr.db'
SECRET_KEY = '1234567890!@#$%^&*()'

USERS = {'admin':'TajneHaslo'}


#############
# Aplikacja #
#############

app = Flask(__name__)
app.config.from_object(__name__)



@app.route('/')
def main():
    all_entries = get_all_entries()
    user_entries = get_user_entries(session['username'])
    return render_template('main.html',
                           all_entries=all_entries,
                           user_entries=user_entries,
                           popular_queries=get_popular_queries())


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        try:
            do_login(request.form['username'],
                     request.form['password'])
        except ValueError:
            flash(u"Błędne dane", 'error')
        else:
            flash(u'Zostałeś zalogowany', 'success')
            return redirect(url_for('main'))
    return render_template('login.html')


@app.route('/logout')
def logout():
    session.clear()
    flash(u'Zostałeś wylogowany')
    return redirect(url_for('main'))


@app.route('/search', methods=['GET','POST'])
def search():
    try:
        query = request.values.get('q', '')
        thumb = request.values.get('thumb', '')
        if not thumb:
            write_entry(session['username'], query)
        data = search_query(query)
        if not data:
            abort(404)
        if request.method == 'POST':
            flash(u'Znaleziono: %s wyników' % data['results'], 'info')
            data['url']=url_for('search', q=query)
            data['tbUrl']=url_for('search', q=query, thumb=True)
            session['found'] = data
            return redirect(url_for('main'))
        else:
            url = data['tbUrl'] if thumb else data['url']
            r = requests.get(url=url)
            resp = make_response(r.content)
            resp.headers['content-type'] = r.headers['content-type']
            return resp
    except sqlite3.Error as e:
        flash(u'Wystąpił błąd: %s' % e.args[0], 'error')
        return redirect(url_for('main'))


################
# Wyszukiwanie #
################

def search_query(query):
    if not query:
        return None
    url = 'https://ajax.googleapis.com/ajax/services/search/images'
    params = dict(v=1.0, q=query)
    resp = requests.get(url=url, params=params)
    data = resp.json()
    if not 'responseData' in data:
        return None
    img = data['responseData']['results'][0]
    return dict(title=img['titleNoFormatting'], url=img['url'], tbUrl=img['tbUrl'], results=data['responseData']['cursor']['estimatedResultCount'])



###############
# Baza danych #
###############

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    g.db.close()


def write_entry(username, query):
    g.db.execute('insert into entries (username, query, datetime) values (?, ?, datetime("now"))',
                 [username, query])
    g.db.commit()


def get_all_entries():
    cur = g.db.execute('select username, query, datetime from entries order by id desc limit 50')
    entries = [dict(username=row[0], query=row[1], datetime=row[2]) for row in cur.fetchall()]
    return entries


def get_user_entries(username):
    cur = g.db.execute('select query, datetime from entries where username=? order by id desc limit 50',[username])
    entries = [dict(query=row[0], datetime=row[1]) for row in cur.fetchall()]
    return entries


def get_popular_queries():
    cur = g.db.execute('select query, count(query) as c from entries group by query order by c desc limit 10')
    queries = [dict(query=row[0], count=row[1]) for row in cur.fetchall()]
    return queries


################
# Autentykacja #
################

def do_login(usr, pwd):
    if not usr in app.config['USERS']:
        raise ValueError
    elif pwd != app.config['USERS'][usr]:
        raise ValueError
    else:
        session.clear()
        session['logged_in'] = True
        session['username'] = usr

@app.before_request
def update_username():
    if not session.get('username'):
        session['username'] = request.remote_addr

################
# Uruchomienie #
################

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)